import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './pages/cart/cart.component';
import { CategoryComponent } from './pages/category/category.component';
import { ClientMainViewComponent } from './pages/client-main-view/client-main-view.component';
import { ErrorComponent } from './pages/error/error.component';
import { HomeComponent } from './pages/home/home.component';
import { ItemComponent } from './pages/item/item.component';
import { SalesFormComponent } from './pages/sales-form/sales-form.component';




const routes: Routes = [

  {
    path: '',
  /*   component: HomeComponent, */
  component: ClientMainViewComponent
  },
  /* {
    path: 'producto/:id/:slug',
    component: ItemComponent,
  },
  {
    path: 'mi-carrito',
    component: CartComponent,
  },
  {
    path: 'categoria/:id',
    component: CategoryComponent,
  },
  {
    path: 'finalizar-compra',
    component: SalesFormComponent,
  },
  {
    path: '**',
    component: ErrorComponent,
  }, */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
