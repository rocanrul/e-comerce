/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { PostalCodeService } from './postal-code.service';

describe('Service: PostalCode', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostalCodeService]
    });
  });

  it('should ...', inject([PostalCodeService], (service: PostalCodeService) => {
    expect(service).toBeTruthy();
  }));
});
