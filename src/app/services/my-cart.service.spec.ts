/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MyCartService } from './my-cart.service';

describe('Service: MyCart', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyCartService]
    });
  });

  it('should ...', inject([MyCartService], (service: MyCartService) => {
    expect(service).toBeTruthy();
  }));
});
