import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Entity } from '../models/entity';
import { Observable } from 'rxjs';
import { AdressModel } from '../models/adress-model';
import { MedioPago } from '../models/medioPago';
import { MedioEnvio } from '../models/medioEnvio';
import {API} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PostalCodeService {

  constructor(private http: HttpClient) { }


  getAdress(code: number) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.get<AdressModel[]>( API + "cp/" + code, { 'headers': headers });
  }

  getStates() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
      .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<Entity[]>( API + "entidades", { 'headers': headers });
  }

  getMedioPago() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
      .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<MedioPago[]>( API + "medios-pago", { 'headers': headers });
  }

  getMedioEnvio() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
      .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<MedioEnvio[]>( API + "medios-envio", { 'headers': headers });
  }


/* FIXME: arregar el tipado tal vez sea por que faltaba import */
  postPurchase(clientData : any){
    let json = (clientData);
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    .append('Access-Control-Allow-Origin', '*');
    return this.http.post( API + "venta", json, { 'headers': headers });
  
  }


  
}

