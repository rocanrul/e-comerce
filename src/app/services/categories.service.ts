import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Categories } from '../models/categories';
//import { SubCategories } from '../models/subCategories';
import { Observable } from 'rxjs';
import {API} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {



constructor(private http: HttpClient) { }


getCategories():Observable <Categories[]>{
  let headers = new HttpHeaders().set('Content-Type','application/json')
  .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<Categories[]>(API + "categorias" ,{'headers': headers});
  }

  /* getSubCategories():Observable <SubCategories[]>{
    let headers = new HttpHeaders().set('Content-Type','application/json')
    .append('Access-Control-Allow-Origin', '*');
      //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
      return this.http.get<SubCategories[]>(this.url + "subcategorias" ,{'headers': headers});
    } */
}
