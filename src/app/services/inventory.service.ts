import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from '../models/item';
import { tap } from 'rxjs/operators';
import {API} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  private isLoading: boolean = false;
  constructor(private http: HttpClient) { }


  
  private setLoading(isLoading: boolean) {
    this.isLoading = isLoading;
  }

  getItems(subCategory: number, pag: number, n: number): Observable<Item[]> {

    if (!this.getLoading) {

      return;
    }

    this.setLoading(true);
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
      .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<Item[]>(API + `productos/${subCategory}/${pag}/${n}`, { 'headers': headers }).pipe(
      tap(() => {
        this.setLoading(false);
      })
    );
  }

  getItem(id: number, slug: string): Observable<Item> {
    
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<Item>( API + `productos/show/${id}/${slug}`);
  }



  getLoading() {



    return this.isLoading.valueOf;

  }
}
