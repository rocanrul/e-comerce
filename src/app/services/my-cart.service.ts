import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DataCartBuy } from '../models/dataCartBuy';
import { ItemCart } from '../models/itemCart';
import {API} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MyCartService {

  
  constructor(private http: HttpClient) { }

  postCart(itemCart: ItemCart[]): Observable <ItemCart[]>{
    let json = (itemCart);
    let headers = new HttpHeaders().set('Content-Type','application/json');
    
    return this.http.post<DataCartBuy[]>(API + 'verify', json, {headers: headers});
  
   }

}
