import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
//import { Item } from '../models/item';
import { ItemCart } from '../models/itemCart';




@Injectable({
  providedIn: 'root'
})
export class CartDataService {

  private dataArray: ItemCart[] = [];
  private dataString = new Subject<string>();
  private data = new Subject<ItemCart>();
  dataObservable$ = this.data.asObservable();
  private sameItem: Boolean = false;

  private testItem: ItemCart = {id:-1,cantidad:0};
  


  dataCart$ = this.dataString.asObservable();

  constructor() {

    if (JSON.parse(localStorage.getItem('my-cart'))) {

      this.dataArray = JSON.parse(localStorage.getItem('my-cart'));
    }

  }


  set the_item(value) {



  }

  onDataChange(id:number, cantidad:number) {

    /*  this.data.next(dataSource); */

/*     this.testArray.concat(this.testArray, [dataSource]); */
  /*   this.testArray.push(dataSource); */

  this.testItem = {id:id, cantidad:cantidad};

  this.sameItem = false; 

    for(let i = 0; i < this.dataArray.length; i++){
      console.log(this.dataArray);
      if(this.dataArray[i].id === id ){
      console.log("este es el tamaño del vector: " + this.dataArray.length);
        console.log("este es el iterador: " + i);
        this.dataArray[i].cantidad += this.testItem.cantidad;
        console.log("el arreglo vale:" + this.dataArray[i].cantidad);
        console.log("el nuevo item vale: " + this.dataArray[i].cantidad);
       console.log("el arreglo ahora vale:" + this.dataArray[i].cantidad);
       this.sameItem = true;
       console.log("son el mismo objeto");
      }
    }
    if(this.sameItem == false){
      console.log("voy a meter un nuevo valor");
      this.dataArray.push(this.testItem);
    }
    
    console.log("este es el servicio", this.dataArray);
    this.setArray();
    
    console.log("esta es la prueba: " , this.testItem);

  }

  private setArray() {

    localStorage.setItem('my-cart', JSON.stringify(this.dataArray));
    this.dataString.next(JSON.stringify(this.dataArray));
    /*   console.log("este es el json", this.dataArray); */
  }


  actArray(dataArray: ItemCart[]) {

    this.dataArray = dataArray;

    this.setArray();

  }

}
