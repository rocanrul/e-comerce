import { NgModule } from '@angular/core';
import { MatFormFieldModule } from "@angular/material/form-field"; /* campos de formulario */
import { MatInputModule } from "@angular/material/input"; /* inputs */
import { MatSelectModule } from '@angular/material/select'; /* selects (input) */
import { MatAutocompleteModule } from '@angular/material/autocomplete'; /* autocompletar input */
import { MatButtonModule } from "@angular/material/button"; /* boton */
import { MatButtonToggleModule } from "@angular/material/button-toggle"; /* boton toggle */
import { MatIconModule } from '@angular/material/icon'; /* iconos */
import { MatBadgeModule } from '@angular/material/badge'; /* notificaciones */
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'; /* progreso */
import { MatToolbarModule } from '@angular/material/toolbar'; /* navbar */
import { MatSidenavModule } from '@angular/material/sidenav';  /* side navbar */ //FIXME: VER COMO IMPLEMENTARLO DE MANERA NATURAL
import { MatMenuModule } from '@angular/material/menu'; /* menu desplegable  y menu desplegable hijo*/
import { MatListModule } from '@angular/material/list';  /* listas  y listas de navegacion (util para side-nav)*/
import { MatDividerModule } from '@angular/material/divider'; /* sirve para crear divisiones (util en listas) */
import { MatGridListModule } from '@angular/material/grid-list'; /* sirve para crear grids */
import { MatExpansionModule } from '@angular/material/expansion'; /* contenido expandible */
import { MatCardModule } from '@angular/material/card'; /* cards */
import { MatTabsModule } from '@angular/material/tabs'; /* contenido separado en pestañas */
import { MatStepperModule } from '@angular/material/stepper'; /* contenido separado en pasos (algo parecido a pestañas util en formularios grandes)  */
import { MatCheckboxModule} from '@angular/material/checkbox';  /* checkboxes */
import { MatRadioModule } from '@angular/material/radio';  /* radioButtons */
import { MatDatepickerModule } from '@angular/material/datepicker';  /* dataPicker */
import { MatNativeDateModule } from '@angular/material/core'; /* uno de los mayores misterios de angular :/  */
import { MatTooltipModule } from '@angular/material/tooltip'; /* un pequeño recuadro con informacion para el usuario debajo de un button al pasar el cursor */
import { MatSnackBarModule } from '@angular/material/snack-bar'; /* un recuadro flotante que sirve para dar una notificacion sobre un evento */
import { MatDialogModule } from '@angular/material/dialog';  /* muestra un cuadro de dialogo */
import { MatTableModule } from '@angular/material/table'; /* tablas */
import { MatFileUploadModule } from 'angular-material-fileupload'; /* subir archivos y arrastrar archivos */




const MATERIALS = [

  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatBadgeModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatSidenavModule,
  MatMenuModule,
  MatListModule,
  MatDividerModule,
  MatGridListModule,
  MatExpansionModule,
  MatCardModule,
  MatTabsModule,
  MatStepperModule,
  MatCheckboxModule,
  MatRadioModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTooltipModule,
  MatSnackBarModule,
  MatDialogModule,
  MatTableModule,
  MatFileUploadModule

]

@NgModule({
  imports: [MATERIALS],
  exports: [MATERIALS]
})
export class MaterialsModule { }
