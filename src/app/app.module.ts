import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from './components/components.module';
import { PagesModule } from './pages/pages.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreManagerModule } from './store-manager/store-manager.module';
import { MaterialsModule } from './materials/materials.module';
import { PagesRoutingModule } from './pages/pages-routing.module';







@NgModule({
  declarations: [
  

  ],
  imports: [
    NgxUsefulSwiperModule,
    BrowserModule,
    StoreManagerModule,
    PagesRoutingModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ComponentsModule,
    PagesModule,
    BrowserAnimationsModule,
  
    MaterialsModule,
  
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
