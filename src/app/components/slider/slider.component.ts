import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { Item } from 'src/app/models/item';
import { Swiper, SwiperOptions } from 'swiper';




@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],

})
export class SliderComponent implements OnInit, AfterViewInit {



  config: SwiperOptions = {
    spaceBetween: 10,
    slidesPerView: 3,
    loop: true,
    freeMode: true,
    loopedSlides: 3, //looped slides should be the same
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
  };

  

  swiper: Swiper;
  galleryThumbs: Swiper;

  @Input() item: Item;
  @Input() autoplay: boolean;
  @Input() arrows: boolean;
  @Input() pagination: boolean;
  @Input() thumbView: boolean;


  constructor() { }

  ngOnInit(): void {


  }

  ngAfterViewInit() {


    this.config.loopedSlides = 2;


  /*   if (this.thumbView) {


      this.galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 3,
        loop: true,
        freeMode: true,
       
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
      });

      this.galleryThumbs.loo
    }
 */
    

    this.swiper = new Swiper('.gallery-top', {
      spaceBetween: 30,
      centeredSlides: true,
      loop: true,
      autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: this.galleryThumbs,
      },
    });
    if (this.autoplay == false) {
      /*  console.log("me intento detener"); */
      this.swiper.autoplay.stop();
      /* this.swiper.navigation.destroy(); */
    }




  }

}
