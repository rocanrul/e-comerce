import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';

import { MenuComponent } from './menu/menu.component';
import { SliderComponent } from './slider/slider.component';
import { TestComponent } from './test/test.component';
import { ProductGridComponent } from './product-grid/product-grid.component';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    MenuComponent,
    SliderComponent,
    TestComponent,
    ProductGridComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    PipesModule
 
  ],

  exports:[
    MenuComponent,
    SliderComponent,
    TestComponent,
    ProductGridComponent,
  ]
})
export class ComponentsModule { }
