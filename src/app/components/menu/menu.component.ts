import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as  $ from 'jquery';
import { Subscription } from 'rxjs';
import { Categories } from 'src/app/models/categories';
import { Item } from 'src/app/models/item';
import { ItemCart } from 'src/app/models/itemCart';
/* import { SubCategories } from 'src/app/models/subCategories'; */
import { ItemComponent } from 'src/app/pages/item/item.component';
import { CartDataService } from 'src/app/services/cart-data.service';
import { CategoriesService } from 'src/app/services/categories.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  cartData: ItemCart;
  cartDataArray: ItemCart[] = [];
  subscription: Subscription;
  categories: Categories[];
  /*  subCategories: SubCategories[]; */
  constructor(private cartDataService: CartDataService, private router: Router, private categoriesService: CategoriesService) {

  /*   this.subscription = cartDataService.dataObservable$.subscribe(
      data => {
        this.cartData = data;
        this.cartDataArray.push(this.cartData);
        localStorage.setItem('my-cart', JSON.stringify(this.cartDataArray));
      }); */

     
  }





  ngOnInit(): void {

    this.categoriesService.getCategories().subscribe(data => {

      this.categories = data;
    });



    this.subscription = this.cartDataService.dataCart$.subscribe(string =>{

      this.cartDataArray = JSON.parse(string);

      console.log("este es el menu", this.cartDataArray);
    });


    if(JSON.parse(localStorage.getItem('my-cart'))){
      this.cartDataArray = JSON.parse(localStorage.getItem('my-cart'));
    }
    /*   
     this.categoriesService.getSubCategories().subscribe(data => {
 
       this.subCategories = data;
       console.log("estas son las sub categorias: " , this.subCategories);
 
     }); */



    /* if (localStorage.getItem('my-cart')) {
      console.log("el localstorage tiene " + localStorage.getItem('my-cart').length);

      if (localStorage.getItem('my-cart').length > 0) {
        console.log(this.cartDataArray);
        this.cartDataArray = JSON.parse(localStorage.getItem('my-cart'));
        console.log(this.cartDataArray);
      }
    } */



  }


  ngAfterContentInit(): void {
    let menuMain = document.getElementById('menu');
    let menuContent = document.getElementById('navbar');

    $(document).ready(function () {

      $(window).scroll(function () {

        if ($(window).scrollTop() >= 100) {

          menuMain.classList.add('hide-menu');
          menuMain.classList.add('deploy-menu');

          menuContent.classList.add('fullsize');

        } else {

          menuMain.classList.remove('hide-menu');
          menuMain.classList.remove('deploy-menu');

          menuContent.classList.remove('fullsize');

        }

      });
    });
  }


  filtrarCategoria() {


  }
}
