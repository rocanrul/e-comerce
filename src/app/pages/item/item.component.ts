import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from 'src/app/models/item';
import { ItemCart } from 'src/app/models/itemCart';
import { CartDataService } from 'src/app/services/cart-data.service';
import { InventoryService } from 'src/app/services/inventory.service';



@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  item: Item;
  amount = 1;
  itemCartData: ItemCart = { id: 0, cantidad: 0 };




  constructor(private inventory: InventoryService,
    private cartDataService: CartDataService,
    private activatedRoute: ActivatedRoute,
    private location: Location) {
  }

  ngOnInit(): void {

    /* obtener id y slug de la url */
    const id = this.activatedRoute.snapshot.params.id
    const slug = this.activatedRoute.snapshot.params.slug

    this.inventory.getItem(id, slug).subscribe(data => {
      this.item = data;
      console.log(this.item);
    });
  }

  addToCart() {

    this.itemCartData.cantidad = this.amount;

    this.itemCartData.id = this.item.id;
    console.log("este es el boton", this.itemCartData);
    //this.item.amountToBuy = this.amount;
    this.cartDataService.onDataChange(this.itemCartData.id, this.itemCartData.cantidad);
  }


  goBack() {

    this.location.back();

  }

}
