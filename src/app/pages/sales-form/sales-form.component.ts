import { Location } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatAccordion } from '@angular/material/expansion';

import { AdressModel } from 'src/app/models/adress-model';
import { ClientData } from 'src/app/models/client-data';
import { DatosCliente } from 'src/app/models/datosCliente';
import { Entity } from 'src/app/models/entity';
import { MedioEnvio } from 'src/app/models/medioEnvio';
import { MedioPago } from 'src/app/models/medioPago';
import { PostalCodeService } from 'src/app/services/postal-code.service';


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sales-form',
  templateUrl: './sales-form.component.html',
  styleUrls: ['./sales-form.component.css']
})
export class SalesFormComponent implements OnInit, AfterViewInit {

  public inputControl: FormControl;
  form: FormGroup = new FormGroup({

  });

  public emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();



  @ViewChild(MatAccordion) accordion: MatAccordion;


  constructor(private postalCode: PostalCodeService,
    private location: Location) { }


  states: Entity[] = [];
  code: number;
  adress: AdressModel[] = [];
  name: string = "";
  lastname: string = "";
  phone: string = "";
  email: string = "";
  mediosPago: MedioPago[];
  mediosEnvio: MedioEnvio[];
  clientData: ClientData = {} as ClientData;
  datosCliente: DatosCliente = new DatosCliente();
  codigo: number;
  colony: string;
  estado: string;
  municipio: string;
  number: string;
  street: string;




  ngOnInit() {

    this.inputControl = new FormControl();

    this.postalCode.getStates().subscribe(resp => {
      this.states = resp;
      console.log(this.states);
    });

    this.postalCode.getMedioPago().subscribe(pago => this.mediosPago = pago);

    this.postalCode.getMedioEnvio().subscribe(envio => this.mediosEnvio = envio);

    this.clientData.productos = JSON.parse(localStorage.getItem('my-cart'));
  }

  ngAfterViewInit() {
  }


  goBack() {
    this.location.back();
  }

  checkAdress() {
    if (this.code != null && this.estado != "" && this.municipio != "" && this.colony != "" && this.street != "" && this.number != "") {
      this.datosCliente.direccion = this.street.concat(
        "," + this.number.concat(
          "," + this.colony.concat(
            "," + this.municipio.concat(
              "," + this.estado.concat(
                "," + this.code)))));
    }
  }

  getAdress(code: number) {


    if (this.code.toString().length == 5) {

      this.postalCode.getAdress(this.code).subscribe(resp => {
        this.adress = resp;
        if (this.adress.length > 1) {
          this.estado = this.adress[0].municipio.entidad.d_estado;
          this.municipio = this.adress[0].municipio.d_mnpio;
          this.colony = "";
        } else if (this.adress.length == 1) {
          this.estado = this.adress[0].municipio.entidad.d_estado;
          this.municipio = this.adress[0].municipio.d_mnpio;
          this.colony = this.adress[0].d_asenta;
        }
      });
    } else if (this.code.toString().length != 5 && this.adress.length > 0) {
      this.adress = [];
      this.estado = "";
      this.municipio = "";
      this.colony = "";
    }
  }




  getMunicipios() {
    /* TODO: */
    alert("lo hare");
  }



  buy() {

    this.clientData.datosCliente = this.datosCliente;

    this.postalCode.postPurchase(this.clientData).subscribe(resp => console.log(resp));
    console.log(this.clientData);

  }





}

