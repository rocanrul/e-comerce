import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


/* import { AppRoutingModule } from '../app-routing.module'; */
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { ItemComponent } from './item/item.component';
import { ComponentsModule } from '../components/components.module';
import { CartComponent } from './cart/cart.component';
import { CategoryComponent } from './category/category.component';
import { SalesFormComponent } from './sales-form/sales-form.component';
import { ClientMainViewComponent } from './client-main-view/client-main-view.component';
import { PagesRoutingModule } from './pages-routing.module';
import { AppComponent } from '../app.component';
import { MaterialsModule } from '../materials/materials.module';





@NgModule({
  declarations: [
    HomeComponent,
    ErrorComponent,
    ItemComponent,
    CartComponent,
    CategoryComponent,
    SalesFormComponent,
    ClientMainViewComponent,
    AppComponent,
   
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    HttpClientModule,
 /*    AppRoutingModule, */
    ComponentsModule,
    FormsModule,
    MaterialsModule,
    
  ],
exports:[
  HomeComponent,
    ErrorComponent,
    ItemComponent,
    CartComponent,
    CategoryComponent,
    SalesFormComponent
]

})
export class PagesModule { }
