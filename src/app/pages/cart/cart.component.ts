import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Item } from 'src/app/models/item';

import { DataCartBuy } from 'src/app/models/dataCartBuy';
import { ItemCart } from 'src/app/models/itemCart';

import { CartDataService } from 'src/app/services/cart-data.service';
import { MyCartService } from 'src/app/services/my-cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  itemCarts: ItemCart[];
  dataCartBuy: DataCartBuy[];
  subscription: Subscription;
  subtotal:number = 0;

  price: number;
  constructor(private myCart: MyCartService, private cartDataService: CartDataService, private router: Router) {


  }

  ngOnInit() {
    this.itemCarts = JSON.parse(localStorage.getItem('my-cart'));

    if (this.itemCarts) {
      this.myCart.postCart(this.itemCarts).subscribe((response: DataCartBuy[]) => {

        this.dataCartBuy = response;
        for (let i = 0; i < this.dataCartBuy.length; i++) {
          if (this.dataCartBuy[i].cantidad < this.dataCartBuy[i].unidadesCliente && this.dataCartBuy[i].cantidad > 0) {
            this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
            this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
            alert("Hubo un cambio de estado en tu carrito, el articulo " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
          }

          if (this.dataCartBuy[i].cantidad == 0) {
            this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
            this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
            alert("Hubo un cambio de estado en tu carrito, el articulo:  " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
          }

          this.subtotal +=  (this.dataCartBuy[i].precio - ((this.dataCartBuy[i].precio) * (this.dataCartBuy[i].descuento / 100))) * this.dataCartBuy[i].unidadesCliente;
        }
        console.log(this.dataCartBuy);
      });




    }

  }

  ngAfterViewInit() {
    /*  this.subscription = this.cartDataService.dataObservable$.subscribe(
       data => {
         this.cartData = data;
         console.log("la respuesta esta en, ", data);
     }); */
  }

 

  remove(id: number) {


/*     for (let i = 0; i < this.dataCartBuy.length; i++) {
      if (this.dataCartBuy[i].id == id) {
     
        this.dataCartBuy.splice(i, 1);
      }
    } */this.subtotal = 0;

    for (let i = 0; i < this.itemCarts.length; i++) {
      if (this.itemCarts[i].id == id) {
      /*   console.log("voy a intentar borrar el elemento: " + id); */
        this.itemCarts.splice(i, 1);
      }
      
    }
  /*   console.log(this.itemCarts); */

    this.cartDataService.actArray(this.itemCarts);

    if (this.itemCarts) {
      this.myCart.postCart(this.itemCarts).subscribe((response: DataCartBuy[]) => {

        this.dataCartBuy = response;
        for (let i = 0; i < this.dataCartBuy.length; i++) {
          if (this.dataCartBuy[i].cantidad < this.dataCartBuy[i].unidadesCliente && this.dataCartBuy[i].cantidad > 0) {
            this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
            this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
            alert("Hubo un cambio de estado en tu carrito, el articulo " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
          }

          if (this.dataCartBuy[i].cantidad == 0) {
            this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
            this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
            alert("Hubo un cambio de estado en tu carrito, el articulo:  " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
          }

          for (let i = 0; i < this.dataCartBuy.length; i++) {
            if (this.dataCartBuy[i].id == id) {
           
              this.dataCartBuy.splice(i, 1);
            }
          }
         
          this.subtotal +=  (this.dataCartBuy[i].precio - ((this.dataCartBuy[i].precio) * (this.dataCartBuy[i].descuento / 100))) * this.dataCartBuy[i].unidadesCliente;
        }
        /* console.log(this.dataCartBuy); */
      });
    }

    

  }

  changeDataValue(id: number, cantidad: number) {
    this.subtotal = 0;
    for (let i = 0; i < this.itemCarts.length; i++) {
      if (this.itemCarts[i].id == id) {

        this.itemCarts[i].cantidad = cantidad;
        this.cartDataService.actArray(this.itemCarts);

        if (this.itemCarts) {
          this.myCart.postCart(this.itemCarts).subscribe((response: DataCartBuy[]) => {
    
            this.dataCartBuy = response;
            for (let i = 0; i < this.dataCartBuy.length; i++) {
              if (this.dataCartBuy[i].cantidad < this.dataCartBuy[i].unidadesCliente && this.dataCartBuy[i].cantidad > 0) {
                this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
                this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
                alert("Hubo un cambio de estado en tu carrito, el articulo " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
              }
    
              if (this.dataCartBuy[i].cantidad == 0) {
                this.dataCartBuy[i].unidadesCliente = this.dataCartBuy[i].cantidad;
                this.changeDataValue(this.dataCartBuy[i].id, this.dataCartBuy[i].cantidad);
                alert("Hubo un cambio de estado en tu carrito, el articulo:  " + this.dataCartBuy[i].nombre + "  no tiene suficiente stock");
              }
    
             
              this.subtotal +=  (this.dataCartBuy[i].precio - ((this.dataCartBuy[i].precio) * (this.dataCartBuy[i].descuento / 100))) * this.dataCartBuy[i].unidadesCliente;
            }
            /* console.log(this.dataCartBuy); */
          });
        }
        
      }
     
    }
  }


  salesFormView(){

    this.router.navigateByUrl("/finalizar-compra");
  }

}
