import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { CategoryComponent } from './category/category.component';
import { ClientMainViewComponent } from './client-main-view/client-main-view.component';
import { ErrorComponent } from './error/error.component';
import { HomeComponent } from './home/home.component';
import { ItemComponent } from './item/item.component';
import { SalesFormComponent } from './sales-form/sales-form.component';

const routes: Routes = [


    {
        path: '',
        /*   component: HomeComponent, */
        component: ClientMainViewComponent,
        children: [
            {
                path: "", component: HomeComponent
            },

            {
                path: 'producto/:id/:slug',
                component: ItemComponent,
            },
            {
                path: 'mi-carrito',
                component: CartComponent,
            },
            {
                path: 'categoria/:id',
                component: CategoryComponent,
            },
            {
                path: 'finalizar-compra',
                component: SalesFormComponent,
            },
            {
                path: '**',
                component: ErrorComponent,
            },
        ]
    },




];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
