import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Item } from 'src/app/models/item';
import { InventoryService } from 'src/app/services/inventory.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})

//VER LOS ITEMS DE UNA CATEGORIA ESPECIFICA
export class CategoryComponent implements OnInit {

  items: Item[];
  route: any;
  constructor(private inventaryService: InventoryService, private activatedRoute: ActivatedRoute, private router: Router) {

   
    if (this.inventaryService.getLoading()) {
      this.inventaryService.getItems(this.activatedRoute.snapshot.params.id, 0, 5).subscribe(data => {
        this.items = data;
      });
    }

  }

  ngOnInit(): void {

  
    this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) =>{ 

      if (this.inventaryService.getLoading()) {
        this.inventaryService.getItems(this.activatedRoute.snapshot.params.id, 0, 1).subscribe(data => {
          this.items = data;
        });
        
      }
    });
    
  }

}
