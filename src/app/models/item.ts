export interface Item {
  id: number;
  nombre: string;
  descripcion: string;
  slug?: string;
  precio: number;
  descuento: number;
  stock: number;
  amountToBuy?: number;
  caracteristicas: [
    {
      id: number;
      nombre: string;
      valor: string;
    }
  ];
  imagenes: [
    {
      id: number;
      nombreImagen: string;
      nombreImagenMin: string;
    }
  ];
}
