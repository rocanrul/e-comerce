export interface DataCartBuy{

    id: number;
    nombre: string;
    precio: number;
    descuento: number;
    cantidad: number;
    nombreImagenMin?: string;
    unidadesCliente: number;
    slug:string;
}