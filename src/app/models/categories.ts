export interface Categories {

    id: number;
    nombreCategoria: string;
    vSubcategorias: [{
        id: number;
        nombre: string;
        catPadre?: number;
        categoria: {
            id: number;
            nombre: string;
        }
    }]
}