export interface AdressModel {

    id: number;
    codigo: number;
    d_asenta: string;
    municipio: {
        id: number;
        d_mnpio: string;
        entidad: {
            id: number;
            d_estado: string;
        }
    }
}