import {Categories} from "../models/categories";

export interface SubCategories{

    id: number;
    nombre: string;
    catPadre?: number;
    categoria:{
        id:number;
         nombre:string;
         
    };
}