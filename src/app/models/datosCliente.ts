export class DatosCliente {
    nombre: string;
    apellido: string;
    direccion: string;
    telefono: string;
    email: string;
}