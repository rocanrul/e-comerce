import { DatosCliente } from "./datosCliente";

export interface ClientData {

 
    datosCliente: DatosCliente;
    medioPago: number;
    medioEnvio: number;
    productos:[{
        id: number;
        cantidad: number;
    }]
}
