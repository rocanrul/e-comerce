import { Image } from "./image";

export interface Item{

    nombre: string;
    descripcion: string;
    slug: string;
    costo: number;
    precio: number;
    descuento?: number;
    codigo: number;
    sku: number;
    stock: number;
    imagenes: Image [];
    subcategoriaId:number;
    subcategoria: Subcategoria;

}

export interface Subcategoria {
    id:        number;
    nombre:    string;
    catPadre:  number;
    categoria: Categoria;
}

export interface Categoria {
    id:     number;
    nombre: string;
}
