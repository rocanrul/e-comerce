export interface IncioInfo {
    ventas_pendientes: string;
    total_ventas: string;
    inventario_total: string;
}
