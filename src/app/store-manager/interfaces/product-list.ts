export interface ProductList {
    id: number;
    nombre: string;
    precio: number;
    codigo: string;
    descuento: number;
    imageMin?: string;
    slug: string;
    stock: number;

}
