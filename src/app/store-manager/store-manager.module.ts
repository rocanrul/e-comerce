import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { StoreManagerRoutingModule } from './store-manager-routing.module';
import { MenuComponent } from './components/menu/menu.component';
import { MainViewComponent } from './views/main-view/main-view.component';
import { AddProductComponent } from './views/add-product/add-product.component';
import { AddCategoryComponent } from './views/add-category/add-category.component';
import { ListProductComponent } from './views/list-product/list-product.component';
import { SalesComponent } from './views/sales/sales.component';
import { InventaryComponent } from './views/inventary/inventary.component';
import { ReportsComponent } from './views/reports/reports.component';
import { SettingsComponent } from './views/settings/settings.component';
import { HomeComponent } from './views/home/home.component';
import { MaterialsModule } from '../materials/materials.module';
import { SaleDetailsComponent } from './views/sale-details/sale-details.component';
import { PipesModule } from '../pipes/pipes.module';
import { MovementComponent } from './components/movement/movement.component';







@NgModule({
  declarations: [
    MenuComponent,
    MainViewComponent,
HomeComponent,
    AddProductComponent,
    AddCategoryComponent,
    ListProductComponent,
    SalesComponent,
    InventaryComponent,
    ReportsComponent,
        SettingsComponent,
        SaleDetailsComponent,
        MovementComponent,
      
  
  ],
  imports: [
    CommonModule,
    StoreManagerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialsModule,
    PipesModule
  
  ]
})
export class StoreManagerModule { }
