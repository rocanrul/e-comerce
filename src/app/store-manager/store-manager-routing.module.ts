import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddCategoryComponent } from './views/add-category/add-category.component';
import { AddProductComponent } from './views/add-product/add-product.component';
import { HomeComponent } from './views/home/home.component';
import { InventaryComponent } from './views/inventary/inventary.component';
import { ListProductComponent } from './views/list-product/list-product.component';
import { LoginComponent } from './views/login/login.component';
import { MainViewComponent } from './views/main-view/main-view.component';
import { ReportsComponent } from './views/reports/reports.component';
import { SaleDetailsComponent } from './views/sale-details/sale-details.component';
import { SalesComponent } from './views/sales/sales.component';
import { SettingsComponent } from './views/settings/settings.component';


const routes: Routes = [
  {path: 'admin-manager', component: LoginComponent},
  {
   
    path: 'admin-manager/:user', component: MainViewComponent,
    children: [
      { path: "", component: HomeComponent },
       { path: "nuevo-producto", component: AddProductComponent },
       { path: "editar-producto/:id/:slug", component: AddProductComponent },
      { path: "nueva-categoria", component: AddCategoryComponent },
      { path: "productos", component: ListProductComponent },
      { path: "ventas", component: SalesComponent},
      { path: "ventas/:id", component: SaleDetailsComponent },
      { path: "inventario", component: InventaryComponent },
      { path: "informes", component: ReportsComponent },
      { path: "ajustes", component: SettingsComponent }, 
      { path: '**', redirectTo: '/admin-manager', pathMatch: 'full' }, 
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreManagerRoutingModule { }
