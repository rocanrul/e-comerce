import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { InventaryService } from '../../services/inventary.service';

@Component({
  selector: 'app-movement',
  templateUrl: './movement.component.html',
  styleUrls: ['./movement.component.css']
})
export class MovementComponent implements OnInit {

  subscription: Subscription;
  option: string = "seleccione una opcion";
  data: number;

  constructor(private inventaryService: InventaryService) { }

  ngOnInit(): void {
    console.log("sasadsad");
    this.subscription = this.inventaryService.data$.subscribe(data =>{
      this.data = data;
      console.log("OBSERVABLE",data);
    })
  }

}
