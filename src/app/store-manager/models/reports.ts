export interface Reports {

    
        productoMasVendido: {
          id: number;
          nombre: string;
          total: number;
        }
        ventasTotales: number;
        productoMasGanancia: {
          id: number;
          nombre: string;
          total: number;
        }
        totalProductosNoVendidos: number;
        gananciasTotales: number
      
}
