import { Image } from "./image";

export interface Item{

    id?: number;
    nombre: string;
    descripcion: string;
     precio: number;
     descuento?: number;
     stock: number;
    slug: string;
    costo: number;
    caracteristicas?: Caracteristicas[]
   

    codigo: number;
    sku: number;
   
    imagenes: Image [];
    subcategoriaId:number;
    subcategoria: Subcategoria;

}

export interface Subcategoria {
    id:        number;
    nombre:    string;
    catPadre:  number;
    categoria: Categoria;
}

export interface Categoria {
    id:     number;
    nombre: string;
}

export interface Caracteristicas {
    id: number;
    nombre: string;
    valor: string;
}
