export interface Sales {

    id:number;
    nombreCliente:string;
    direccionCliente:string;
    madioPago:string;
    MedioEnvio: string;
    fecha: Date;
    total: number;
    productos: string[];

}
