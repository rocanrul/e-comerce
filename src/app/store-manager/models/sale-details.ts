export interface SaleDetails {
  id: number;
  nombreCliente: string;
  direccionCliente: string;
  telefonoCliente: string;
  emailCliente: string;
  medioPago: MedioPago;
  medioEnvio: MedioEnvio;
  fecha: Date;
  subtotal: number;
  total: number;
  articulosVenta: ArticulosVenta[];
}

export interface ArticulosVenta {
  id: number;
  nombre: string;
  cantidad: number;
  precio: number;
}

export interface MedioEnvio {
  id: number;
  nombre: string;
  costo: number;
}

export interface MedioPago {
  id: number;
  nombre: string;
}
