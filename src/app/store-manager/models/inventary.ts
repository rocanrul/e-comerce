export interface Inventary {
    id:        number;
    nombre:    string;
    codigo:    string;
    sku:       string;
    proveedor: null;
    stock:     number;
    nombreImagenMin: string;
}
