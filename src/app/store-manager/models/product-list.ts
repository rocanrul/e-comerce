export interface ProductList {
    id: number;
    nombre: string;
    precio: number;
    codigo: string;
    descuento: number;
    nombreImagenMin?: string;
    slug: string;
    stock: number;

}
