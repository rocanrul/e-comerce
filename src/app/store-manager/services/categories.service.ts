import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Categories } from '../models/categories';
import {API} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {


  constructor(private http: HttpClient) { }

  
  
  getCategories() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.get<Categories[]>( API + "categorias", { 'headers': headers });
  }

}
