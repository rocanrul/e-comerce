import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../models/item';
import { NewItem } from '../models/new-item';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url = 'api/';

  constructor(private http: HttpClient) { }


  
  getProduct(id: number, slug: string): Observable<Item> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
      .append('Access-Control-Allow-Origin', '*');
    //obtiene todos los datos json dentro de la url  y los guarda en un arreglo
    return this.http.get<Item>( `${this.url}productos/show/${id}/${slug}`, { 'headers': headers });
  }


  postNewProduct(product: Item) {
    let json = product;
    let headers = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.post<NewItem>( this.url + "producto", json, { 'headers': headers });
  }

  putEditProduct(product: Item) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.put<NewItem>( this.url + "producto", product, { 'headers': headers });
  }



  postImage(image: any, idProducto: number){
    const FORMDATA: FormData = new FormData();
    
    /*  let headers = new HttpHeaders().set('Content-Type', 'undefined')  */
   /*  let headers = new HttpHeaders(); */
   let headers = new HttpHeaders().set('Content-Type', 'application/json');
    FORMDATA.append('file', image, image.name);
    FORMDATA.append('idProducto', idProducto.toString());
    return this.http.post(this.url + "producto/imagen", FORMDATA);
  }

  deleteImage(id:number): Observable <any>{
    return this.http.delete(`${this.url}producto/imagen/${id}`);
  }

}


