import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Reports } from '../models/reports';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  url = 'api/';

  constructor(private http: HttpClient) { }

  getReports() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<Reports>(this.url + "informes", { headers: headers });
  }
}
