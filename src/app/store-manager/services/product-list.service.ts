import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductList } from '../models/product-list';

@Injectable({
  providedIn: 'root'
})
export class ProductListService {

  url = 'api/';

  constructor(private http: HttpClient) { }

  getProductList() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.get<ProductList[]>( this.url + "productos", { 'headers': headers });
  }

}
