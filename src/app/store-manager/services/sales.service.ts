import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Sales } from '../models/sales';
import { SaleDetails } from '../models/sale-details';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SalesService {
  url = 'api/';

  constructor(private http: HttpClient) {}

  getSales() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<Sales[]>(this.url + "ventas", { headers: headers });
  }

  getSaleDetails(id: number) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<SaleDetails>(`${this.url}ventas/show/${id}`, {
      headers: headers,
    });
  }

  putSale(id: number, state: number) {
    let json = {
      id,
      state,
    };

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.put<any>(this.url + 'ventas', json, { headers: headers });
  }


  putStatus(status: number, id: number){
    let json = {estado: status, id: id}
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.put<any>(`${this.url}/ventas`, json, {headers: headers});
    
  }
}
