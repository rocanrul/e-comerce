import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IncioInfo } from '../models/incio-info';
import { Observable } from 'rxjs';
import {API} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InicioService {

  

  constructor(private http: HttpClient) { }

  
  
  getCategories(): Observable <IncioInfo> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.get<IncioInfo>( API + "inicioback", { 'headers': headers });
  }
}
