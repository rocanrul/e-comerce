import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inventary } from '../models/inventary';
import { Movement } from '../models/movement';
import { Subject } from 'rxjs';
import {API} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InventaryService {

  private data = new Subject<number>();
  data$ = this.data.asObservable();

 

  constructor(private http: HttpClient) { }

  getInventary():Observable<Inventary[]> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get<Inventary[]>( API + "inventario", { 'headers': headers });
  }

  putStock(json: Movement){

    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.put<Movement>( API + "inventario", json, { 'headers': headers });

  }


  onDataChange(id: number){
    console.log("servicio funciona", id);
    this.data.next(id);
  }


}
