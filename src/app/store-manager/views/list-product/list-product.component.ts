import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductList } from '../../models/product-list';
import { ProductListService } from '../../services/product-list.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {

  productLists: ProductList[];

  constructor(private router: Router, private productListService: ProductListService) { }

  ngOnInit(): void {

    this.productListService.getProductList().subscribe(productLists => {
      this.productLists = productLists;
      console.log(this.productLists);
    });
  }

  newProduct(){
    this.router.navigateByUrl("admin-manager/user/nuevo-producto");
  }

  editProduct(id: number, slug:string){
    this.router.navigateByUrl(`admin-manager/user/editar-producto/${id}/${slug}`);
    //this.router.navigateByUrl(admin-manager/user/editar-producto/id/321");
  }

}
