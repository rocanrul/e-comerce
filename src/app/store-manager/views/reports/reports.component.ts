import { Component, OnInit } from '@angular/core';
import { Reports } from '../../models/reports';
import { ReportsService } from '../../services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  reports: Reports;

  constructor(private reportsService: ReportsService) { }

  ngOnInit(): void {

    this.reportsService.getReports().subscribe(reports => {
      this.reports = reports;
      console.log('Este es el reporte ',this.reports);
    });
  }

}
