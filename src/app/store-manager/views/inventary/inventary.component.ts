import { Component, OnInit } from '@angular/core';

import { Inventary } from '../../models/inventary';
import { Movement } from '../../models/movement';
import { InventaryService } from '../../services/inventary.service';
import { MatDialog } from '@angular/material/dialog';
import { MovementComponent } from '../../components/movement/movement.component';


@Component({
  selector: 'app-inventary',
  templateUrl: './inventary.component.html',
  styleUrls: ['./inventary.component.css']
})
export class InventaryComponent implements OnInit {

  inventaryList: Inventary[];

 

  movement: Movement = {} as Movement;

  
  displayedColumns: string[] = ['image' ,'id', 'name', 'code', 'sku' , 'provider', 'stock', 'movement'];



  constructor(
    private invetaryService: InventaryService,
    private dialog : MatDialog,
    ) { }

  ngOnInit(): void {
  

    this.invetaryService.getInventary().subscribe(inventary =>{
      this.inventaryList = inventary;
      console.log(this.inventaryList);
    })
  }

  aceptMovement(id: number){
    

   let inputValue = (<HTMLInputElement>document.getElementById(id.toString())).value;
   this.movement.id = id;
   this.movement.cantidad = parseInt(inputValue);
   this.movement.tipo = 2;
   this.invetaryService.putStock(this.movement).subscribe(resp => {
     console.log(resp);
  });

   }


   showMovementView(){
     this.invetaryService.onDataChange(1);
    this.dialog.open(MovementComponent);

   }


}
