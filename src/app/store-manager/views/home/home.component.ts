import { Component, OnInit } from '@angular/core';
import { IncioInfo } from '../../models/incio-info';
import { InicioService } from '../../services/inicio.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  notifications: IncioInfo;
  constructor(private InicioService: InicioService) { }

  ngOnInit(): void {

    this.InicioService.getCategories().subscribe(info => this.notifications = info);
  }

}
