import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Sales } from '../../models/sales';
import { SalesService } from '../../services/sales.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {

  sales: Sales[];
  constructor(private selesService: SalesService, private router: Router) { }

  ngOnInit(): void {
    this.selesService.getSales().subscribe(sales => {
      this.sales = sales;
      console.log(this.sales);
    });
  }

  

  viewSaleDetails(id: number){
    console.log(this.router.url);
    this.router.navigateByUrl(this.router.url + "/" + id);
  }

}
