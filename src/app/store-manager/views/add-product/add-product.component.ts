import { Component, OnInit } from '@angular/core';
import { Categories } from '../../models/categories';
import { Item } from '../../models/item';
import { CategoriesService } from '../../services/categories.service';
import { ProductsService } from '../../services/products.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Image } from '../../models/image';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  fileToUpload: File = null;

  product: Item = {} as Item;

  image: Image = {} as Image;
  categories: Categories[];
  category: number = -1;
  thumbnails: any[] = [];
  imageList: Image [];

  id;
  slug;
  url;

  constructor(
    private productService: ProductsService,
    private categoriesService: CategoriesService,
    private _snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params.id;
    this.slug = this.activatedRoute.snapshot.params.slug;

    if (this.id > 0 && this.slug != undefined) {
      this.productService
        .getProduct(this.id, this.slug)
        .subscribe((product) => {
          this.product = product;
          this.category = this.product.subcategoria.categoria.id;
          this.product.subcategoriaId = this.product.subcategoria.id;

          if (this.product.imagenes.length > 0) {
            this.imageList = this.product.imagenes;
          }

          console.log(this.product);
        });
    }else{
      this.id = 0;
    }

    this.categoriesService
      .getCategories()
      .subscribe((categories) => {
        this.categories = categories;
        console.log(this.categories);
      });
    this.product.imagenes = [];
  }

  upload() {
    this.productService.postImage(this.fileToUpload, this.id).subscribe(
      (data) => {
        this.image.nombreImagen = this.fileToUpload.name;
        this.product.imagenes.push(this.image);
        this.thumbnails.push(this.fileToUpload);
        console.log('esto es la leche', this.thumbnails[0].stream());
        this.openSnackBar(
          'Se ha subido la imagen correctamente',
          'Aceptar'
        );
        this.image = {} as Image;
        console.log('este es el producto', this.product);
      },
      (error) => {
        this.openSnackBar(
          'No se pudo subir la imagen' ,
          'Aceptar'
        );
      }
    );
  }

  uploadProduct() {
    if (this.id > 0 && this.slug != undefined) {
      this.product.id = this.id;
      this.productService.putEditProduct(this.product).subscribe((resp) => {
        console.log(resp);
      });
    } else {
      this.productService.postNewProduct(this.product).subscribe((resp) => {
        console.log(resp);
        console.log(this.product);
        this.openSnackBar(
          'Se ha agregado el articulo: ' + resp.nombre,
          'Aceptar'
        );
      });
    }
  }

  deleteImage(id: number){
    this.productService.deleteImage(id).subscribe(resp => console.log(resp));
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  handleFileInput(event, files: FileList) {
    this.fileToUpload = files.item(0);
    this.upload();

    var reader = new FileReader();

    reader.onload = (event: any) => {
      this.url = event.target.result;
      this.thumbnails.push(this.url);
    };

    reader.onerror = (event: any) => {
      console.log('File could not be read: ' + event.target.error.code);
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  thumbs(event) {}
}
