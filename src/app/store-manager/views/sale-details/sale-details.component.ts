import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SaleDetails } from '../../models/sale-details';
import { SalesService } from '../../services/sales.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-sale-details',
  templateUrl: './sale-details.component.html',
  styleUrls: ['./sale-details.component.css']
})
export class SaleDetailsComponent implements OnInit {

  saleDetails: SaleDetails;

  message: {
    mensaje: string;
  }

  cost: number;
  delivery: number = 150;
  constructor(
    private salesService: SalesService,
    private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,) {
    
   }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id

    this.salesService.getSaleDetails(id).subscribe(details => {
      this.saleDetails = details;
      console.log(this.saleDetails);
      this.cost = this.saleDetails.articulosVenta[0].precio
      for(let i = 1; i < this.saleDetails.articulosVenta.length; i++){

        this.cost += this.saleDetails.articulosVenta[i].precio
      }
    });
  }

  updateStatus(){
    this.salesService.putStatus(2, this.activatedRoute.snapshot.params.id).subscribe(message => {
      this.message = message;
      this.openSnackBar(this.message.mensaje,'Aceptar');
  
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

}
