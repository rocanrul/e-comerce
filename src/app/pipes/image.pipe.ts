import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {

  transform(img: string ): string {

    if(img){
      return `http://192.168.43.130:8080/img/${img}`;
    }else{
      return './assets/no-image.jpg';
    }
    
   
  }

}
